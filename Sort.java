import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.NumberFormatException;
import java.io.File;
import java.io.FileNotFoundException;

public class Sort{
    public static void main(String[]args)throws IOException{
        int selection1,selection2,n=1,i;
        Object x;
        Object[][]Input= new Object[6][n];
        boolean ulang=true;
        boolean repeat = true,Repeat=true;
        do{
            selection1=Menu.TopMenu();
            switch(selection1){
                case 1:
                    String Name=read.FileName()+".txt";
                    read.reader(Name);
                    do{
                        for(i=0;i<=5;i++){
                            x=write.writeBarang(i);
                            write.WriteTxt((String)x, Name);
                            Input[i][n-1]=x;
                        }
                        System.out.println("Jika ingin melanjutkan ketik 1");
                        System.out.print("Jika ingin mengakhiri ketik 0");
                        int Select=write.selection();
                        if(Select==0){
                            ulang=false;
                            n=n+1;
                        }
                        else{
                            ulang=true;
                        }
                    }
                    while(ulang);
                    break;
                case 2:
                    selection2=Menu.Menu2();
                    switch(selection2){
                    case 1:
                        for(i=0;i<n;i++){
                            sort.bubbleSort(n, (Object[][]) Input[i][n-1],selection2);
                        }
                        break;
                    case 2:
                        for(i=0;i<n;i++){
                            sort.bubbleSort(n, (Object[][]) Input[i][n-1],selection2);
                        }
                        break;
                    case 3:
                        for(i=0;i<n;i++){
                            sort.bubbleSort(n, (Object[][]) Input[i][n-1],selection2);
                        }
                        break;
                    case 4:
                        for(i=0;i<n;i++){
                            sort.bubbleSort(n, (Object[][]) Input[i][n-1],selection2);
                        }
                        break;
                    case 5:
                        for(i=0;i<n;i++){
                            sort.bubbleSort(n, (Object[][]) Input[i][n-1],selection2);
                        }
                        break;
                    default:
                        repeat=true;
                    }

                break;
            case 0:
                System.out.println("Terima Kasih!");
                break;
            default:
                System.out.println("Inputan Salah !");
                repeat = false;
                break;
            }
        }
        while(repeat);
        System.out.println("Terima Kasih!");
    }
}

class write{
    public static String selectionText()throws IOException{
	BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
	String select = scan.readLine();
	return select;
    }
    public static int selection()throws IOException{
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        int Select=0;
        boolean repeat = false;
        do{
            String select = scan.readLine();
            try{
                Select= Integer.parseInt(select);
                repeat = false;
            }
            catch(NumberFormatException e){
                System.out.println("Inputan Anda Salah !");
                repeat = true;
            }
        }
        while(repeat);
    return Select;
    }  
    public static void WriteTxt(String x,String path) throws IOException{
        String[] Input= new String[6];
        int i=1;
        Input[i-1]=x;
        try{
            File file = new File(path);
            if(!file.exists()){
                file.createNewFile();
            }
            FileWriter filewrite = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bufferedwriter = new BufferedWriter(filewrite);
            bufferedwriter.newLine();
            bufferedwriter.write(Input[i-1]);
            i=i+1;
            bufferedwriter.close();
            }
            catch(Exception e){
                System.out.println(e);
            }

    }
    public static Object writeBarang(int i) throws IOException{
        Object x;
        Barang.Barang(i);
        if(i==5)
            return Barang.garis();
        else
            return x=write.selectionText();
    }
}

class read{
    public static void reader(String fileName) throws IOException{
        String line = null;
        try{
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while((line=bufferedReader.readLine()) != null){
                System.out.println(line);
            }
        }   
        catch(FileNotFoundException ex){
            System.out.println("Unable to open file '"+ fileName +"'");
        }
        catch(IOException ex){
            System.out.println("Error reading file '"+fileName+"'");
        }
  }
    public static String FileName() throws IOException{
        System.out.print("Nama file : ");
        String name = write.selectionText();
        return name;
    }
}

class Menu{
    public static int TopMenu()throws IOException{
        System.out.println("------Program Sederhana------");
        System.out.println("1. Input Data ");
        System.out.println("2. Kriteria");
        System.out.println("0. Keluar");
        int selection1 = write.selection();
        return selection1;
    }
    public static int Menu2()throws IOException{
        System.out.println("Kriteria Berdasarkan :");
        System.out.println("1. Kode");
        System.out.println("2. Barang");
        System.out.println("3. Warna");
        System.out.println("4. Jumlah");
        System.out.println("5. Harga");
        int selection2 = write.selection();
        return selection2;
    }
}

class Barang{
    public static void Barang(int i) throws IOException{
        
            if(i==0){
                System.out.print("Kode Barang : ");
            }
            if(i==1){
                System.out.print("Nama Barang : ");
            }
            if(i==2){
                System.out.print("Warna Barang : ");
            }
            if(i==3){
                System.out.print("Jumlah Barang : ");
            }
            if(i==4){
                System.out.print("Harga Barang : ");
            }
    }    
    public static Object garis(){
        Object garis="-----";
        return garis;
    }
}

class sort{
    public static void bubbleSort(int n,Object [][]Input,int choice){
        int stage,y,x;
        String[]newInput=new String[n];
        for(int i=0;i<n;i++){
            newInput[i]=(String) Input[i][(choice)];
        }
        Object tmp;
            for(stage=0;stage<6;stage++){
                for(y=0;y<6;y++){
                    if(newInput[y].compareTo(newInput[y+1])>0){
                    tmp=Input[y][n];
                    Input[y][n]=Input[y+1][n];
                    Input[y+1][n]=tmp;
                    }
                }
            }
    }
}